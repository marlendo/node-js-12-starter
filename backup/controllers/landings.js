require('dotenv').config()
const axios = require('axios')
const response = require('../helper');
const con = require('../db');
const ENV = process.env.NODE_ENV;

let url = 'http://localhost:3001/';

if (ENV == 'local') {
    url = 'http://157.230.42.171:3001/'
}

async function request({
    method, url, headers, data
}) {
    const payload = {
        headers,
        method,
        url,
        data
    }

    const res = await axios(payload);
    return res.data;
}

exports.welcome = async function (req, res) {
    try {
        const data = "welcome to ucc admin api"
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getLanding = async function (req, res) {
    try {
        let data = {};
        let uccProfile = {};
        let uccSlider = [];
        let uccAbout = {};
        let uccFeatures = {};

        const dataProfile = await con.conLanding("select * from ucc_profile");
        const dataSlider = await con.conLanding("select * from ucc_slider");
        const uccMenu = await con.conLanding("select * from ucc_menu where ucc_menu.hidden = 0 order by ucc_menu.order");
        const dataAbout = await con.conLanding("select * from ucc_about");
        const dataFeatures = await con.conLanding("select * from ucc_feature");

        uccProfile = JSON.parse(dataProfile[0].profile)
        uccAbout = {
            img: dataAbout[0].img,
            content: JSON.parse(dataAbout[0].content)
        }

        for (let row of dataSlider) {
            uccSlider.push({
                id: row.id,
                img: row.img,
                text: {
                    title: row.title,
                    content: JSON.parse(row.content),
                }
            })
        }

        uccFeatures = {
            img: dataFeatures[0].img,
            left: JSON.parse(dataFeatures[0].left_content),
            right: JSON.parse(dataFeatures[0].right_content)
        }

        data = {
            uccProfile,
            uccSlider,
            uccMenu,
            uccAbout,
            uccFeatures
        }
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getLandingMenu = async function (req, res) {
    try {
        const uccMenu = await con.conLanding("select * from ucc_menu where ucc_menu.hidden = 0 order by ucc_menu.order");
        response.success(res, uccMenu)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getProfile = async function (req, res) {
    try {
        const dataProfile = await con.conLanding("select * from ucc_profile");
        const payload = JSON.parse(dataProfile[0].profile);
        response.success(res, payload)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.putProfile = async function (req, res) {
    try {
        const payload = req.body;
        const sql = "update ucc_profile set profile = '" + JSON.stringify(payload) + "'";
        const data = await con.conLanding(sql);
        if (data) {
            const dataProfile = await con.conLanding("select * from ucc_profile");
            const payload = JSON.parse(dataProfile[0].profile);
            response.success(res, {
                payload,
                history: data
            })
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getSlider = async function (req, res) {
    try {
        const dataSlider = await con.conLanding("select * from ucc_slider");
        let uccSlider = [];
        for (let row of dataSlider) {
            uccSlider.push({
                id: row.id,
                img: row.img,
                text: {
                    title: row.title,
                    content: JSON.parse(row.content),
                }
            })
        }
        response.success(res, uccSlider)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.postSlider = async function (req, res) {
    try {
        const title = req.body.title;
        const img = req.body.img == null ? null : "'" + req.body.img + "'";
        const content = req.body.content;
        const sql = "INSERT INTO ucc_slider (`id`, `img`, `title`, `content`) VALUES (NULL, " + img + ", '" + title + "', '" + content + "')";
        const data = await con.conLanding(sql);
        if (data) {
            let uccSlider = [];
            const dataSlider = await con.conLanding("select * from ucc_slider");
            for (let row of dataSlider) {
                uccSlider.push({
                    id: row.id,
                    img: row.img,
                    text: {
                        title: row.title,
                        content: JSON.parse(row.content),
                    }
                })
            }
            response.success(res, {
                payload: uccSlider,
                history: data
            })
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.putSlider = async function (req, res) {
    try {
        const id = req.body.id;
        const title = req.body.title;
        const img = req.body.img == null ? null : "'" + req.body.img + "'";
        const content = req.body.content;
        const sql = "update ucc_slider set title = '" + title + "', img = " + img + ", content = '" + content + "' where id = " + id;
        const data = await con.conLanding(sql);
        if (data) {
            let uccSlider = [];
            const dataSlider = await con.conLanding("select * from ucc_slider");
            for (let row of dataSlider) {
                uccSlider.push({
                    id: row.id,
                    img: row.img,
                    text: {
                        title: row.title,
                        content: JSON.parse(row.content),
                    }
                })
            }
            response.success(res, {
                payload: uccSlider,
                history: data
            })
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getMenu = async function (req, res) {
    try {
        let uccMenu = await con.conLanding("select * from ucc_menu order by ucc_menu.order");
        for(let i in uccMenu){
            uccMenu[i].hidden = uccMenu[i].hidden == 0 ? false : true
        }
        response.success(res, uccMenu)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.postMenu = async function (req, res) {
    try {
        const idmasterAdminAccess = req.body.idmasterAdminAccess;
        const img = req.body.img == null ? null : "'" + req.body.img + "'";
        const title = req.body.title;
        const order = req.body.order;
        const sql = "INSERT INTO `ucc_menu` (`id`, `idmasterAdminAccess`, `img`, `title`, `order`, `hidden`) VALUES " +
            "(NULL, '" + idmasterAdminAccess + "', " + img + ", '" + title + "', '" + order + "', 0)";
        const data = await con.conLanding(sql);
        if (data) {
            response.success(res, data)
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.putMenu = async function (req, res) {
    try {
        const id = req.body.id
        const img = req.body.img == null ? null : "'" + req.body.img + "'";
        const title = req.body.title;
        const order = req.body.order;
        const hidden = req.body.hidden ? 1 : 0;
        const sql = "UPDATE `ucc_menu` SET `img` = " + img +
            ", `title` = '" + title + "', `hidden` = " + hidden + ", `order` = '" + order + "' WHERE `ucc_menu`.`id` = " + id;
        const data = await con.conLanding(sql);
        if (data) {
            response.success(res, data)
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getFeature = async function (req, res) {
    try {
        const dataFeatures = await con.conLanding("select * from ucc_feature");
        let uccFeatures = {
            img: dataFeatures[0].img,
            left: JSON.parse(dataFeatures[0].left_content),
            right: JSON.parse(dataFeatures[0].right_content)
        }
        response.success(res, uccFeatures)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.putFeature = async function (req, res) {
    try {
        const img = req.body.img == null ? null : "'" + req.body.img + "'";
        const left = req.body.left;
        const right = req.body.right;
        const sql = "UPDATE `ucc_feature` SET `img` = " + img +
            ", `left_content` = '" + left +
            "', `right_content` = '" + right + "'";
        const data = await con.conLanding(sql);
        if (data) {
            const dataFeatures = await con.conLanding("select * from ucc_feature");
            let uccFeatures = {
                img: dataFeatures[0].img,
                left: JSON.parse(dataFeatures[0].left_content),
                right: JSON.parse(dataFeatures[0].right_content)
            }
            response.success(res, {
                payload: uccFeatures,
                history: data
            })
        }
    } catch (err) {
        response.failed(res, err)
    }
};

exports.getAbout = async function (req, res) {
    try {
        const dataAbout = await con.conLanding("select * from ucc_about");
        let uccAbout = {
            img: dataAbout[0].img,
            content: JSON.parse(dataAbout[0].content)
        }
        response.success(res, uccAbout)
    } catch (err) {
        response.failed(res, err)
    }
};

exports.putAbout = async function (req, res) {
    try {
        const img = req.body.img == null ? null : "'" + req.body.img + "'";
        const content = req.body.content;
        const sql = "UPDATE `ucc_about` SET `img` = " + img +
            ", `content` = '" + content + "'";
        const data = await con.conLanding(sql);
        if (data) {
            const dataAbout = await con.conLanding("select * from ucc_about");
            response.success(res, {
                payload: {
                    img: dataAbout[0].img,
                    content: JSON.parse(dataAbout[0].content)
                },
                history: data
            })
        }
    } catch (err) {
        response.failed(res, err)
    }
};

async function getContent(req, id){
    try {

        let payload = req.uccPayload;
        payload.body = {
            idGroupContent: id
        }

        const data = await request({
            method: 'post',
            url: url + 'inquirylandingPageContent',
            headers: null,
            data: payload
        })
        return data.values;   
    } catch (error) {
        console.log(error)
    }    
}

exports.pageContent = async function (req, res) {
    try {
        let payload = req.uccPayload;
        payload.body = {
            idmasterAdminAccess: req.params.id
        }

        const getHeader = await request({
            method: 'post',
            url: url + 'inquirylandingPageGroupContentByIdAdminAccs',
            headers: null,
            data: payload
        })                

        const content = [];
        if(getHeader.values[0].idGroupContent !== undefined){            
            for(let i in getHeader.values){
                let data = getHeader.values[i];
                const children = await getContent(req, data.idGroupContent)                 
                data.child = children;
                content.push(data)
            }
        }
        response.success(res, content)
    } catch (err) {
        response.failed(res, err)
    }
};