exports.failed = function (res, err) {
    console.log(err);
    res.status(200).json({
        error: true,
        message: "ExceptionError",
        data: err
      });
}

exports.success = function (res, data) {
    res.status(200).json({
        error: false,
        message: "success",
        data
      });
}