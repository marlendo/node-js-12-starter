module.exports = function (app) {    
    const indexController = require('../controllers');

    app.route('/').get(indexController.welcome)    

};
