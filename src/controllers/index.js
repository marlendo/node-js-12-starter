require('dotenv').config()
const response = require('../helper');
const con = require('../db');
const ENV = process.env.NODE_ENV;

// async function request({
//     method, url, headers, data
// }) {
//     const payload = {
//         headers,
//         method,
//         url,
//         data
//     }

//     const res = await axios(payload);
//     return res.data;
// }

exports.welcome = async function (req, res) {
    try {
        const data = 'Welcome to Home Services API Backend'
        response.success(res, data)
    } catch (err) {
        response.failed(res, err)
    }
};