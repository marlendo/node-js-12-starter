exports.failed = function (res, err) {
    console.log(err);
    res.status(200).json({
        error: true,
        errorCode: null,
        message: "Internal Server Error",
        data: err
      });
}

exports.success = function (res, data) {
    res.status(200).json({
        error: false,
        errorCode: null,
        message: "success",
        data
      });
}