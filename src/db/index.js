require('dotenv').config()
const mariadb = require('mariadb');
const poolLanding = mariadb.createPool({
    host: process.env.HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    connectionLimit: 5
});

exports.db = async function (query) {
    let conn;
    try {

        conn = await poolLanding.getConnection();
        const execute = await conn.query(query);
        return execute

    } catch (err) {
        throw err;
    } finally {
        if (conn) conn.release(); //release to pool
    }
}